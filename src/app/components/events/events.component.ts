import { Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  events: any = [];

  constructor(private _eventsService: EventService) { }

  ngOnInit() {
    this._eventsService.getEvents().subscribe(
      res => this.events = res,
      err => console.log(err)
    );
  }

}
